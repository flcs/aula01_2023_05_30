//npm init -y
//npm i -D jest
//npm test

const { array } = require("yargs")

class Calculadora {
    display
    operacoesvalidas = ["+", "-", "/", "*"]
    error = ""
    operacao
    anterior

    constructor() {}

    informarValor(value) {
        if (value != undefined)
            this.display = value.toString()
        else
            this.display = ''
    }

    informarOperacao(operacao){
        this.anterior = this.display
        this.display = ''

        if (operacao != undefined){
            if(this.operacoesvalidas.includes(operacao)){
                this.operacao = operacao
            }else{
                this.error = "operacaoinvalida" 
                this.operacao = ''
            }            
         } else{
            this.error = 'operacaonaoinformada'
            this.operacao = ''
        }
    }

    calcular(){
        switch(this.operacao) {
            case '+':
                this.display = parseInt(this.anterior) + parseInt(this.display);
            break;

            case '-':
                this.display = parseInt(this.anterior) - parseInt(this.display);
            break;
        }

        this.display = this.display.toString();
    }
}

module.exports = { Calculadora };