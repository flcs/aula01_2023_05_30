// import Calculadora from "./calculadora";
const { Calculadora } = require('./calculadora');

describe('Calculadora usuário informar valor', () => {
    it('deve infromar um valor e este aparecer no display', () => {
        // Configurar
        const calculadora = new Calculadora();
        // Executar
        calculadora.informarValor(1);
        // Verificar
        expect(calculadora.display).toEqual('1');
    });

    it('Deve aparecer nada se informar um valor nulo', () => {
        // Configurar
        const calculadora = new Calculadora();
        // Executar
        calculadora.informarValor();
        // Verificar
        expect(calculadora.display).toEqual('');
    });

});

describe('Informar operação', () => {
    it('deve informar uma operação + (soma)', () => {
        // Configurar
        const calculadora = new Calculadora();
        // Executar
        calculadora.informarOperacao('+');
        // Verificar
        expect(calculadora.operacao).toEqual('+');
    });

    it('deve informar operação inválida',() => {
        // Configurar
        const calculadora = new Calculadora();
        // Executar
        calculadora.informarOperacao('####');
        // Verificar
        expect(calculadora.error).toEqual('operacaoinvalida');
        expect(calculadora.operacao).toEqual('');
    });
    it('deve rejeitar operacao nula',() => {
        // Configurar
        const calculadora = new Calculadora();
        // Executar
        calculadora.informarOperacao();
        // Verificar
        expect(calculadora.error).toEqual('operacaonaoinformada');
        expect(calculadora.operacao).toEqual('');
    });
});

describe('Informar segundo valor', () => {
    it('deve infromar um valor e este aparecer no display', () => {
        // Configurar
        const calculadora = new Calculadora();
        // Executar
        calculadora.informarValor(1);
        calculadora.informarOperacao('+');
        calculadora.informarValor(2);
        // Verificar
        expect(calculadora.anterior).toEqual('1');
        expect(calculadora.operacao).toEqual('+');
        expect(calculadora.display).toEqual('2');
    });

});

describe('Calcular', () => {
    it('deve calcular a soma', () => {
        // Configurar
        const calculadora = new Calculadora();
        // Executar
        calculadora.informarValor(1);
        calculadora.informarOperacao('+');
        calculadora.informarValor(2);
        calculadora.calcular()
        // Verificar
        expect(calculadora.display).toEqual('3');
    });

    it('deve calcular a subtração', () => {
        // Configurar
        const calculadora = new Calculadora();
        // Executar
        calculadora.informarValor(2);
        calculadora.informarOperacao('-');
        calculadora.informarValor(1);
        calculadora.calcular()
        // Verificar
        expect(calculadora.display).toEqual('1');
    });

});


// export {}